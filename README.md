# Odoo v8 Community Backports Docker Image

This is the Git repo for my own [Docker image](https://cloud.docker.com/repository/docker/johanmcos/odoo) that uses the code from OCA's [Odoo Backports repo](https://github.com/OCA/OCB/tree/8.0).

## Usage
See the Docker Hub page for the full readme on how to use this Docker image and for information regarding contributing and issues.

## Notes

The [python docker image](https://hub.docker.com/_/python) (Debian Stretch version)
is used as the base

pip is used to install the dependencies from
the requirements.txt file included in the git repo

Note that the code is downloaded as a Zip from github, and then verified with a signature. If the source repo is updated, builds will fail until the signature is updated.

## Feedback

If you have question, comments, issues, etc. feel free to [email me](mailto:johan@base10tech.com)

## License

This project is licensed under the GNU AGPLv3. see the [LICENSE](LICENSE) file for details
