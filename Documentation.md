# Odoo 8 With Odoo Community Backports

This is the my own image for Odoo version 8 that uses the code from OCA's [Odoo Backports repo](https://github.com/OCA/OCB/tree/8.0).

I made it for my own use, but anyone is welcome to use, modify, distribute, etc. this image as long as it is understood that it is provided AS-IS (see "License" section)

I include information/recommendations for how the image could be run in production, but I'm not recommending that course of action (see "Support" section)

## How to Use This Image for Testing/Development

This image is purposely designed to copy the usage of [Odoo's official docker images](https://github.com/docker-library/docs/tree/master/odoo) almost exactly, in fact several parts of this are only lightly modified copies of theirs (see "Acknowledgments" section).

The instructions from their page can be copied almost exactly. The only significant difference, as far as I'm aware, is that my image uses Debian Stretch, and thus defaults to using the version 9.6 postgresql-client package, so if you want everything to work correctly, use postgres:9.6 as your database container rather than 10 as their instructions suggest.

If you're working with an existing database that's not in version 9.6, please see my [github repo](https://github.com/johanmcos/docker) for instructions on how to modify this image to suit your needs, it's actually quite easy.

## Production Use

I use this image in production and have had success in doing so.

### PostgreSQL
Running the python code in a container isn't an issue, but be aware that there's an active debate about whether or not running a postgreSQL server in a container is actually good for production use. It may be better to play it safe and use a "real" database server, either bare-metal, VM, or using a system container like LXC/LXD

If you'd like more info, I recommend [this blog post](https://vsupalov.com/database-in-docker/) as a starting point for why it's bad, what situations are worse than others, etc.

I currently run my production database in a container and have yet to experience a problem, but this is on a single container host with a small database that gets frequent backups, but I'm by no means recommending that other people do so.

### Security
Be aware that there's a gaping vulnerability in how the backend of the server utilizes passwords.

Odoo has a done a good job on their part, the server saves passwords in the database with strong security by default (SHA256+salt I believe), but the way the backend works (I believe it's due to WSGI), all passwords will appear in plaintext in /var/lib/odoo/sessions. It's present in every setup, including this one, and not something Odoo could fix even if they wanted to as far as I know.

Without additional exploits (which could exist) you'd need root access to the server to access them, but remember that for any computer system physical access=root access

So far I have mitigated this in three ways:
 1. using external authentication through Oauth (other options exist as well). This way the session files will contain an essentially worthless randomly generated token instead of a static password.

  I use Google, but Odoo offers a system as well (this is why it's not an issue for their cloud-hosted version, and why I recommend that route for people that don't want to deal with these types of concerns)

  In my view, this is a great solution because it's one less password for users to remember.
 2. using completely random passwords. The password disclosure in itself isn't the real issue, with the kind of access you need to exploit it, you can already do whatever you want to the database.

  Instead, the core issue is password reuse, if someone uses the same password for Odoo as they do for e.g. Gmail, online banking, etc., an attacker could use their email and password to login and compromise those accounts.

  For the simplest solution, I recommend using something like [RANDOM.ORG's password generator](https://www.random.org/passwords/) with the full 24 characters, since your users are just going to save it anyways. Of course, I don't recommend their site for generating highly sensitive passwords (and neither do they).
 3. mounting a tmpfs at /var/lib/odoo/sessions. This is more of a "band-aid" but simple enough that I'd recommend everyone do it. A tmpfs is only stored in RAM, it means that every time your server is turned off, the session files will disappear. Logins won't be saved to disk and there won't be recoverable data on any hard drive.

 Keep in mind this doesn't stop anyone from getting the passwords while the server is running, it just clears them when it's shut down or rebooted. It also means, of course, that all sessions will be removed and users will have to log back in, but I don't see that as an issue personally.

## Support
There isn't any. I'll probably keep this up-to-date, but I make no guarantees. If you want support/reliability/service, I recommend Odoo's cloud hosted SaaS version

I use this image, and I want it to be available for others, but I'm by no means suggesting it's a good idea in production use. Free<sup>1</sup> isn't necessarily the cheapest option when you're talking about production use. Things will go wrong (see [Murphy's Law](https://en.wikipedia.org/wiki/Murphy%27s_law)) and you'll have no one to fall back on but yourself and perhaps kind strangers on forums if you're lucky.

<details><summary>[1]</summary>
<p>
When I say "free" in this context, I mean "free as in beer", like running your code on your own server without any fixed cost. "free as in freedom" is always good, and one of the main reasons I like Odoo so much is their use of free software licenses
</p>
</details>

## Acknowledgments
The code contained in this image comes from Odoo and the Odoo Community Association, the files used to build it are adapted from those used by [Odoo's official images](https://github.com/odoo/docker) hosted on [their Dockerhub](https://hub.docker.com/_/odoo/)

### About Odoo
Per [their website](https://www.odoo.com/):
> Odoo (formerly OpenERP) is a suite of open source business apps that cover all your company needs: CRM, eCommerce, accounting, inventory, point of sale, project management, etc.

At a technical training several years ago, I had the pleasure of getting to meet some of the people that work there and certainly had a great impression. I've never personally used their SaaS version, but considering that it's running on the same code base as the server version, I assume it's probably very good.

Note that I have no formal association with their company (past or present) but rather I'm just a satisfied user.

### About the Odoo Community Association (OCA)
Per [their website](https://odoo-community.org/):
> The Odoo Community Association, or OCA, is a nonprofit organization whose mission is to promote the widespread use of Odoo and to support the collaborative development of Odoo features

I've used multiple add-ons from [their Github repo](https://github.com/oca), and the code contained in this container comes from their ["backports" project](https://github.com/OCA/OCB) that provides continued updates for deprecated versions



## Feedback

If you have question, comments, issues, etc. feel free to [email me](mailto:johan@base10tech.com)

## License

Copyright (C) 2019  Johan M. Cos

> <p>This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.</p>
> <p>This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.</p>
> <p>You should have received a copy of the GNU Affero General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.</p>

In addition:
1.  Odoo versions up to and including 8 are licensed under the GNU AGPLv3 as well.

2.  Certain elements included in the software are subject to other licenses or restrictions.

3.  As with all Docker images, these likely also contain other software which may be under other licenses (such as Bash, etc from the base distribution, along with any direct or indirect dependencies of the primary software being contained).

4.  As for any pre-built image usage, it is the image user's responsibility to ensure that any use of this image complies with any relevant licenses for all software contained within.

5.  Please see [LICENSE](LICENSE) for more info
