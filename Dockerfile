###############################################################################
# Copyright (C) 2019 Johan M. Cos <johan@mod10tech.com>                       #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU Affero General Public License as published by #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU Affero General Public License for more details.                         #
#                                                                             #
# You should have received a copy of the GNU Affero General Public License    #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
###############################################################################

FROM python:2.7-stretch
MAINTAINER Johan M. Cos <johan@mod10tech.com>

WORKDIR /usr/src/odoo

#switch postgresql version to 9.5

RUN apt-get update; \
    apt-get install -y --no-install-recommends \
            wget \
            node-less \
            node-clean-css \
            postgresql-client \
            unzip \
            libldap2-dev \
            libsasl2-dev; \
    rm -rf /var/lib/apt/lists/*;

RUN wget https://github.com/OCA/OCB/archive/8.0.zip; \
    echo '27307f308bb245ff408cef88c9f71d07c00c309a 8.0.zip' | sha1sum -c -; \
    unzip *.zip; \
    rm *.zip; \
    mv OCB-8.0/* ./; \
    rm -R OCB-8.0;

RUN pip install --no-cache-dir -r requirements.txt;

RUN useradd odoo; \
    chown -R odoo:odoo /usr/src/odoo; \
    mkdir /var/lib/odoo/files; \
    mkdir /var/lib/odoo/sessions; \
    chown -R odoo:odoo /var/lib/odoo; \
    mkdir /mnt/extra-addons; \
    chown -R odoo:odoo /mnt/extra-addons; \
    mkdir /etc/odoo; \
    chown -R odoo:odoo /etc/odoo;

VOLUME ["/var/lib/odoo/files", "/var/lib/odoo/sessions", "/mnt/extra-addons"]

# Expose Odoo services
EXPOSE 8069 8071

COPY --chown=odoo ./openerp-server.conf /etc/odoo/

COPY ./entrypoint.sh ./

# Set the default config file
ENV OPENERP_SERVER /etc/odoo/openerp-server.conf

# Set default user when running the container
USER odoo:odoo

ENTRYPOINT ["./entrypoint.sh"]

CMD ["openerp-server"]
